#pragma once

#define usage_check(usage, n) if(argc < n+1){\
    fprintf(stderr, "Usage: %s %s\n", argv[0], usage);\
    exit(1);\
}
