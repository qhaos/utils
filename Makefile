BIN := strlen hexof bf
CC := cc
CFLAGS := -O2 -Wall

all: $(BIN)

strlen: strlen.c
	$(CC) strlen.c $(CFLAGS) -o strlen

hexof: hexof.c
	$(CC) hexof.c $(CFLAGS) -o hexof

bf: bf.c
	$(CC) bf.c -O2 -w -o bf

clean:
	rm $(BIN)
