#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"

int main(int argc, char* argv[]) {
    usage_check("[string]", 1);
    printf("%lu\n", strlen(argv[1]));
    return 0;
}
