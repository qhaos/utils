#include <stdio.h>
#include <stdlib.h>
#include "util.h"

int main(int argc, char* argv[]) {
	usage_check("[char]", 1);

	printf("%x\n", argv[1][0]);

	return 0;
}
